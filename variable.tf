variable "AWS_ACCESS_KEY" {}

variable "AWS_SECRET_KEY" {}

variable "AWS_REGION" {}

variable "AWS_AMI" {}

variable "INSTANCE_TYPE" {}

variable "AVAILABILITY_ZONE" {}
  
variable "KEY_NAME" {}


        