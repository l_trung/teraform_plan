resource "aws_instance" "my_instance" {
    ami = "var.AWS_AMI"
    instance_type = "var.INSTANCE_TYPE"

    # the VPC subnet
    subnet_id = "aws_subnet.main-public-1.id"

    # the security group
    vpc_security_group_ids = ["aws_security_group.allow-ssh.id"]

    # the public SSH key
    key_name = "var.KEY_NAME"
}